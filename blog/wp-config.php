<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'darksidedistribution_wp');

/** MySQL database username */
define('DB_USER', 'darkside');

/** MySQL database password */
define('DB_PASSWORD', 'd%&xER32ab&');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*DmCAl{M-*HL6AX}G7L*u(=?UaaQyR:BRxp)y6ZGLW(S9S~3g:hwXv7a#_CVHPt^');
define('SECURE_AUTH_KEY',  '% .E3Dm}$9gP646tD&-gsHp-A>}I!`XhhPnStGoY/Y?MT>P@Xb<HEEtv#,vgi][P');
define('LOGGED_IN_KEY',    'brXPJyi$+rp1FgFf({Hx,WN&q9B)AJ73<l$Q0:>`ANK<Yr#j;@2*>Lwgmc5)&y5]');
define('NONCE_KEY',        'R4qYIPefnWyhZgBeKN=[ozef&:SIW;-PudWJ|ER8`9iJ9n>Z#|Rxbr#`vu>TkY%g');
define('AUTH_SALT',        ':5Sa#!H<+L?n+zVN>r5z/MmJ$f{ne$C_vz_)x0$BG=&H,),qP/FZw*t:`:<y6yVj');
define('SECURE_AUTH_SALT', '=?<Y) #kHL(bQ6p>.KA${(a >,d;Ze6bt8uiFZv>YI]#))8rH]$/T<!J3LEC%`{h');
define('LOGGED_IN_SALT',   'T4 M53/3zk6XHAy,j[aQUX$e_T-Q{6OsuA<gF]l%T_|,z9*)*H)c1B:q%orKu%[g');
define('NONCE_SALT',       '1b&e{Ky L%@<a;hi0q,o[yK0HiiLQ%Y1?zR8#ECmy$X U$Z:1hw.C!kZFIyiZ0IB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
